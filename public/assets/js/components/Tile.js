var Tile = React.createClass({
    getInitialState: function () {
        return {
            unit: this.props.unit !== undefined ? this.props.unit : null,
            tile: {
                rowNumber: this.props['rowId'],
                tileNumber: this.props['tileId'],
                rowTileNumber: this.props['rowTileId']
            }
        };
    },
    getUnit: function () {
        if (this.props.unit && this.props.unit !== 'hidden') {
            return (
                <Unit
                key={this.props['tileId']} unit={this.state.unit}
                Game={this.props.Game}
                />
            );
        }
    },
    handleMouseOver: function () {
        if (this.state.unit) {
            // TODO: Play sound and fade in/out unit opacity.
        }
    },
    handleMouseOut: function () {

    },
    handleClick: function () {
        this.props.Game.handleCurrentState(this.state);
    },
    render: function () {
        var clickHandler = (function () {
        }());
        var classes = 'tile row-' + this.props['rowId'] + ' tile-' + this.props['tileId'];

        if (this.props.unit && this.props.unit !== 'hidden') {
            classes += ' has-unit';
        }

        if (this.props.unit === 'hidden') {
            classes += ' void';
        } else {
            clickHandler = this.handleClick;
        }

        clickHandler = this.handleClick;

        return (
            <div
            className={classes}
            data-row-id={this.props['rowId']}
            data-tile-id={this.props['tileId']}
            data-row-tile-id={this.props['rowTileId']}
            onMouseOver={this.handleMouseOver}
            onMouseOut={this.handleMouseOut}
            onClick={clickHandler}
            >
                {this.getUnit()}
            </div>
        );
    }
});
<?php

namespace App\Tao\Units;

class Cleric extends Unit
{
    public function __construct()
    {
        $this->name = 'Cleric';
        $this->hp = 24;
        $this->power = $this->getPower('heal', 12, false);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(0, 0, 0);
        $this->recovery = 1;
        $this->movement = $this->getMovement('normal', 3);

        return $this;
    }
}

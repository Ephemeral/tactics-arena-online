<div
    class="
        tile
        tile-{{ $number + 1 }}
        @if($tile === 1)
            border
        @endif
    "
    @if($unit !== '')
        data-unit="{{ $unit }}"
    @endif
    data-tile="{{ $number + 1 }}">
    @if($unit !== '')
        {{ $unit }}
    @else
        {{ $number + 1 }}
    @endif
</div>
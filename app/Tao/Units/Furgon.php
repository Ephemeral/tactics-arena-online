<?php

namespace App\Tao\Units;

class Furgon extends Unit
{
    public function __construct()
    {
        $this->name = 'Furgon';
        $this->hp = 38;
        $this->power = $this->getPower('summon', 0, true);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(50, 25, 0);
        $this->recovery = 1;
        $this->movement = $this->getMovement('normal', 3);

        return $this;
    }
}

<?php

namespace App\Http\Controllers;

use App\Tao\Units\Cleric;
use App\Tao\Units\DarkMagicWitch;
use App\Tao\Units\Enchantress;
use App\Tao\Units\Knight;
use App\Tao\Units\Pyromancer;
use App\Tao\Units\Scout;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tao\Units\Assassin;

class PlayController extends Controller
{
    public function arena(User $user)
    {
        // TODO: Fetch currently logged in player's units.
        return view('play.index')->with([
            'user' => $user->first()
        ]);
    }
}

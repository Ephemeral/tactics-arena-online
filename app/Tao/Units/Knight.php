<?php

namespace App\Tao\Units;

class Knight extends Unit
{
    public function __construct()
    {
        $this->name = 'Knight';
        $this->hp = 50;
        $this->power = $this->getPower('damaging', 22, true);
        $this->attack = 1;
        $this->armor = 25;
        $this->blocking = $this->getBlocking(80, 40, 0);
        $this->recovery = 1;
        $this->movement = $this->getMovement('normal', 3);

        return $this;
    }
}

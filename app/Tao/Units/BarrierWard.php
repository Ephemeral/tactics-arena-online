<?php

namespace App\Tao\Units;

class BarrierWard extends Unit
{
    public function __construct()
    {
        $this->name = 'Barrier Ward';
        $this->hp = 32;
        $this->power = $this->getPower('barrier', 0, false);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(100, 100, 100);
        $this->recovery = 2;
        $this->movement = $this->getMovement('immovable', 0);

        return $this;
    }
}

<?php

namespace App\Tao\Units;

class PoisonWisp extends Unit
{
    public function __construct()
    {
        $this->name = 'Poison Wisp';
        $this->hp = 34;
        $this->power = $this->getPower('poison', 2, false);
        $this->attack = 1;
        $this->armor = 0;
        $this->blocking = $this->getBlocking(0, 0, 0);
        $this->recovery = 2;
        $this->movement = $this->getMovement('teleport', 6);

        return $this;
    }
}

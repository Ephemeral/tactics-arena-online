var Arena = React.createClass({
    getInitialState: function () {
        var game = new Game();

        return {
            Game: game
        };
    },
    getTiles: function () {
        var rows = this.props['grid'];
        var id = 0;
        var tiles = [];

        for (var row = 0; row < rows.length; row++) {
            for (var tile = 0; tile < rows[row].length; tile++) {
                tiles.push(
                    <Tile
                    key={id++}
                    rowId={row + 1}
                    tileId={id}
                    rowTileId={tile + 1}
                    unit={rows[row][tile].unit}
                    Game={this.state.Game}
                    />
                );
            }
        }

        return tiles;
    },
    getGameState: function () {
        return this.state.Game.state;
    },
    updateGameState: function (state) {
        this.state.Game.setState(state);
    },
    render: function () {
        var tiles = this.getTiles();

        return (
            <div className="tiles">
                {tiles.map(function (tile) {
                    return tile;
                })}
            </div>
        );
    }
});
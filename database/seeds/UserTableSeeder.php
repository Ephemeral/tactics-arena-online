<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $units = [
            -1 => 'hidden',
            0 => null,
            1 => 'Assassin',
            2 => 'BarrierWard',
            3 => 'BeastRider',
            4 => 'Cleric',
            5 => 'DarkMagicWitch',
            6 => 'DragonSpeakerMage',
            7 => 'DragonTyrant',
            8 => 'Enchantress',
            9 => 'FrostGolem',
            10 => 'Furgon',
            11 => 'GolemAmbusher',
            12 => 'Knight',
            13 => 'LightningWard',
            14 => 'MudGolem',
            15 => 'PoisonWisp',
            16 => 'Pyromancer',
            17 => 'Scout',
            18 => 'StoneGolem',
        ];

        $setup = [
            [-1, -1, 0, 0, 0, 4, 0, 0, 0, -1, -1],
            [-1, 0, 5, 16, 0, 0, 0, 16, 8, 0, -1],
            [0, 1, 0, 0, 12, 12, 12, 0, 0, 17, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 0, 12, 12, 12, 0, 0, 17, 0],
            [-1, 0, 5, 16, 0, 0, 0, 16, 8, 0, -1],
            [-1, -1, 0, 0, 0, 4, 0, 0, 0, -1, -1],
        ];

        $grid = [];

        foreach ($setup as $row => $tiles) {
            foreach ($tiles as $tile) {
                $grid[$row][] = $this->getTile($units[$tile]);
            }
        }

        DB::table('users')->truncate();

        App\User::create([
            'name' => 'Enijar',
            'units' => $grid,
            'email' => 'TheEnijar@gmail.com',
            'password' => Hash::make('password'),
        ]);
    }

    private function getTile($unit = null)
    {
        $tile = new StdClass;

        if (is_null($unit)) {
            $tile->unit = null;
        } elseif ($unit === 'hidden') {
            $tile->unit = 'hidden';
        } else {
            $class = "App\\Tao\\Units\\{$unit}";
            $tile->unit = new $class();
        }

        return $tile;
    }
}
